/**
 * @file
 * Inline media form actions JS code for actions buttons.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Process inline_media_form_actions elements.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches inlineMediaFormActions behaviors.
   */
  Drupal.behaviors.inlineMediaFormActions = {
    attach: function (context, settings) {
      var $actionsElement =
        $(context)
          .find('.inline-media-form-dropdown')
          .once('inline-media-form-dropdown');

      // Attach event handlers to toggle button.
      $actionsElement.each(function () {
        var $this   = $(this);
        var $toggle = $this.find('.inline-media-form-dropdown-toggle');

        $toggle.on('click', function (e) {
          e.preventDefault();
          $this.toggleClass('open');
        });

        $this.on('focusout', function (e) {
          setTimeout(function () {
            if ($this.has(document.activeElement).length === 0) {
              // The focus left the action button group, hide actions.
              $this.removeClass('open');
            }
          }, 1);
        });
      });
    }
  };

})(jQuery, Drupal);
