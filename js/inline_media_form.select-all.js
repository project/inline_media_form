/**
 * @file inline_media_form.view.js
 *
 * This file is a nearly identical copy of the file media_library.view.es6.js
 * from the media_library module.
 */
(($, Drupal) => {
  /**
   * Adds checkbox to select all items in the library.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behavior to select all media items.
   */
  Drupal.behaviors.MediaLibrarySelectAll = {
    attach(context) {
      // Select the root element of the view whether it is the root element of
      // the context or a descendent of the root element of the context. This
      // fixes the following bug which is present in media_library.view.es6.js:
      // https://www.drupal.org/project/drupal/issues/3226628
      //
      // This fix is based on the following suggestion:
      // https://stackoverflow.com/a/5332377/715866
      const viewSelector = '.js-media-library-view[data-view-display-id="widget"]';
      const $view = $(context)
        .filter(viewSelector)
        .add($(viewSelector, context))
        .once('inline-media-form-select-all');

      if ($view.length && $view.find('.js-media-library-item').length) {
        const $checkbox = $(Drupal.theme('checkbox')).on(
          'click',
          ({ currentTarget }) => {
            // Toggle all checkboxes.
            const $checkboxes = $(currentTarget)
              .closest('.js-media-library-view')
              .find('.js-media-library-item input[type="checkbox"]');
            $checkboxes
              .prop('checked', $(currentTarget).prop('checked'))
              .trigger('change');
            // Announce the selection.
            const announcement = $(currentTarget).prop('checked')
              ? Drupal.t('All @count items selected', {
                  '@count': $checkboxes.length,
                })
              : Drupal.t('Zero items selected');
            Drupal.announce(announcement);
          },
        );
        const $label = $(
          '<label class="media-library-select-all"></label>',
        ).text(Drupal.t('Select all media'));
        $label.prepend($checkbox);
        $view
          .find('.js-media-library-item')
          .first()
          .before($label);
      }
    },
  };
})(jQuery, Drupal);
