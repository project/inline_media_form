<?php

/**
 * @file
 * Hook documentation for the Inline Media Form module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter actions, drop-down options, and toolbars exposed by Inline Media Form.
 *
 * This hook affects the header actions and toolbar at the top of an IMF widget.
 * If you are looking to alter options for an individual media file/delta in the
 * widget, implement hook_imf_widget_delta_actions_alter instead.
 *
 * @param array $header_actions
 *   A reference to the contents of the header actions:
 *     - actions - The default actions, which are always visible.
 *     - dropdown_actions - Actions that appear in the drop-down
 *       sub-component.
 *     - toolbar - The toolbar for providing input to the active tool (e.g.,
 *       bulk rename).
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - form: The form structure to which widgets are being attached. This may be
 *     a full entity form structure, or a sub-element of a larger entity form.
 *   - widget: The widget plugin instance.
 *   - items: The field values, as a
 *     \Drupal\Core\Field\FieldItemListInterface object.
 *   - element: The form element render array containing the basic properties
 *     for the top-level widget.
 *   - form_state: The current state of the entity edit form.
 *   - is_translating: Boolean that indicates if the widget is being used to
 *     facilitate translation.
 */
function hook_imf_widget_header_actions_alter(array &$header_actions,
                                              array $context) {
  if (isset($header_actions["actions"]["edit_all"])) {
    // Change the "Edit all" button text to "Open all".
    $header_actions["actions"]["edit_all"]['#value'] = t('Open all');
  }
}

/**
 * Alter actions and drop-down options exposed by Inline Media Form.
 *
 * This hook affects only the options for a single media file row in an IMF
 * widget.
 *
 * @param array $widget_actions
 *   A reference to the array of 'actions' and 'dropdown_actions':
 *     - actions - The default actions, which are always visible.
 *     - dropdown_actions - Actions that appear in the drop-down sub-component.
 * @param array $context
 *   An associative array containing the following key-value pairs:
 *   - form: The form structure to which widgets are being attached. This may be
 *     a full entity form structure, or a sub-element of a larger entity form.
 *   - widget: The widget plugin instance.
 *   - items: The field values, as a
 *     \Drupal\Core\Field\FieldItemListInterface object.
 *   - delta: The order/ordinal position of this item in the array of items (0,
 *     1, 2, etc).
 *   - element: The form element render array containing the basic properties
 *     for the widget.
 *   - form_state: The current state of the entity edit form.
 *   - wrapper: The media item adapter wrapper around the media file for this
 *     widget.
 *   - is_translating: Boolean that indicates if the widget is being used to
 *     facilitate translation.
 *   - allow_reference_changes: Boolean that indicates whether media files can
 *     be re-ordered, added, or removed.
 */
function hook_imf_widget_delta_actions_alter(array &$widget_actions,
                                             array $context) {
  if (isset($widget_actions["actions"]["edit_button"])) {
    // Change the "Edit" button text to "Open".
    $widget_actions["actions"]["edit_button"]["#value"] = t('Open');
  }
}

/**
 * @} End of "addtogroup hooks".
 */
