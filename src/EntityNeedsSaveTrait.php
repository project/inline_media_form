<?php

namespace Drupal\inline_media_form;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Trait for entities that can be marked dirty (i.e. have unsaved changes).
 *
 * Adapted from \Drupal\entity_reference_revisions\EntityNeedsSaveTrait.
 *
 * @see \Drupal\inline_media_form\EntityNeedsSaveInterface
 */
trait EntityNeedsSaveTrait {

  /**
   * Whether the entity needs to be saved or not.
   *
   * @var bool
   */
  protected $saveNeeded = FALSE;

  /**
   * {@inheritdoc}
   */
  public function isSaveNeeded(): bool {
    return $this->saveNeeded;
  }

  /**
   * {@inheritdoc}
   */
  public function markSaveNeeded(): void {
    $this->saveNeeded;
  }

  /**
   * {@inheritdoc}
   */
  public function markSaved(): void {
    $this->saveNeeded = FALSE;
  }

  /**
   * On save, clears the flag that indicates this entity needs to be saved.
   *
   * @see \Drupal\Core\Entity\EntityInterface::postSave()
   * @noinspection PhpDocSignatureInspection
   */
  public function postSave(EntityStorageInterface $storage,
                           $update = TRUE): void {
    parent::postSave($storage, $update);

    $this->markSaved();
  }

}
