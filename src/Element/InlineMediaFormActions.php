<?php

namespace Drupal\inline_media_form\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for a Inline Media Form actions.
 *
 * IMF actions support two types:
 * - actions - The default actions, which are always visible.
 * - dropdown_actions - Actions that appear in the drop-down sub-component.
 *
 * Usage example:
 *
 * @code
 * $form['actions'] = [
 *   '#type'            => 'inline_media_form_actions',
 *   'actions'          => $actions,
 *   'dropdown_actions' => $dropdown_actions,
 * ];
 * $dropdown_actions['button'] = [
 *   '#type' => 'submit',
 * ];
 * @endcode
 *
 * @FormElement("inline_media_form_actions")
 */
class InlineMediaFormActions extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#pre_render' => [
        [static::class, 'preRenderInlineMediaFormActions'],
      ],
      '#theme' => 'inline_media_form_actions',
    ];
  }

  /**
   * Pre render callback for #type 'inline_media_form_actions'.
   *
   * @param array $element
   *   Element array of a #type 'inline_media_form_actions'.
   *
   * @return array
   *   The processed element.
   */
  public static function preRenderInlineMediaFormActions(array $element) {
    $element['#attached']['library'][] = 'inline_media_form/actions';

    if (!empty($element['dropdown_actions'])) {
      foreach (Element::children($element['dropdown_actions']) as $key) {
        $dropdown_action = &$element['dropdown_actions'][$key];

        if (isset($dropdown_action['#ajax'])) {
          $dropdown_action = RenderElement::preRenderAjaxForm($dropdown_action);
        }
        if (empty($dropdown_action['#attributes'])) {
          $dropdown_action['#attributes'] = [
            'class' => ['inline-media-form-dropdown-action'],
          ];
        }
        else {
          $dropdown_action['#attributes']['class'][] =
            'inline-media-form-dropdown-action';
        }
      }
    }

    return $element;
  }

}
