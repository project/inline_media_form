<?php

namespace Drupal\inline_media_form\EntitySummarizer;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface;

/**
 * Default factory for entity summarizers.
 */
class EntitySummarizerFactory implements EntitySummarizerFactoryInterface {

  // ===========================================================================
  // Constants
  // ===========================================================================

  /**
   * The entity summarizers to consult.
   *
   * @var string[]
   */
  private const IMPLEMENTATIONS = [
    EntitySummarizer::class,
  ];

  // ===========================================================================
  // Member Fields
  // ===========================================================================

  /**
   * The repository of entity display information.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Factory for getting summarizers for different types of fields.
   *
   * @var \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface
   */
  protected $fieldSummarizerFactory;

  // ===========================================================================
  // Constructor
  // ===========================================================================

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The repository of entity display information.
   * @param \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface $field_summarizer_factory
   *   The factory to consult for summarizers of fields.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_display_repository,
                              FieldSummarizerFactoryInterface $field_summarizer_factory) {
    $this->entityDisplayRepository = $entity_display_repository;
    $this->fieldSummarizerFactory  = $field_summarizer_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummarizerFor(EntityInterface $entity): EntitySummarizerInterface {
    foreach (static::IMPLEMENTATIONS as $class_name) {
      // @codingStandardsIgnoreLine
      /** @noinspection PhpUndefinedMethodInspection */
      if ($class_name::canHandle($entity)) {
        return new $class_name(
          $this->entityDisplayRepository,
          $this->fieldSummarizerFactory
        );
      }
    }

    throw new \InvalidArgumentException(
      'Unsupported entity type: ' . $entity->getEntityTypeId()
    );
  }

}
