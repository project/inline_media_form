<?php

namespace Drupal\inline_media_form\EntitySummarizer;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface;

/**
 * Default summarizer for entities.
 */
class EntitySummarizer implements EntitySummarizerInterface {

  // ===========================================================================
  // Member Fields
  // ===========================================================================

  /**
   * The repository of entity display information.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Factory for getting summarizers for different types of fields.
   *
   * @var \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface
   */
  protected $fieldSummarizerFactory;

  // ===========================================================================
  // Static Methods
  // ===========================================================================

  /**
   * {@inheritdoc}
   */
  public static function canHandle(EntityInterface $entity): bool {
    // Accept content entities of all types.
    return ($entity instanceof ContentEntityInterface);
  }

  // ===========================================================================
  // Constructor
  // ===========================================================================

  /**
   * Constructor for EntitySummarizer.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The repository of entity display information.
   * @param \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerFactoryInterface $field_summarizer_factory
   *   The factory to consult for summarizers of fields.
   */
  public function __construct(EntityDisplayRepositoryInterface $entity_display_repository,
                              FieldSummarizerFactoryInterface $field_summarizer_factory) {
    $this->entityDisplayRepository = $entity_display_repository;
    $this->fieldSummarizerFactory  = $field_summarizer_factory;
  }

  // ===========================================================================
  // Public Methods
  // ===========================================================================

  /**
   * {@inheritdoc}
   */
  public function summarize(EntityInterface $entity,
                            string $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): string {
    $field_summaries     = $this->summarizeFields($entity);
    $non_empty_summaries = array_filter($field_summaries);

    return implode(', ', $non_empty_summaries);
  }

  /**
   * {@inheritdoc}
   */
  public function summarizeFields(EntityInterface $entity,
                                  $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): array {
    $summaries = [];

    assert($entity instanceof ContentEntityInterface);

    $form_display =
      $this->getEntityDisplayRepository()->getFormDisplay(
        $entity->getEntityTypeId(),
        $entity->bundle(),
        $form_mode
      );

    $components = $form_display->getComponents();

    uasort($components, [SortArray::class, 'sortByWeightElement']);

    foreach (array_keys($components) as $field_name) {
      $field_definition = $entity->getFieldDefinition($field_name);

      // Components can be extra fields; field may not really exist.
      if ($field_definition !== NULL) {
        $field_values = $entity->get($field_name);

        // We do not add content to the summary from empty or base fields
        // (except "name").
        $is_name_field = ($field_name == 'name');
        $is_non_base   = ($field_definition instanceof FieldConfigInterface);
        $is_empty      = $field_values->isEmpty();
        $can_view      = $field_values->access('view');

        if (($is_name_field || $is_non_base) && !$is_empty && $can_view) {
          $summarizer    = $this->getSummarizerFor($field_definition);
          $field_summary = $summarizer->summarize($field_values);

          if (!empty($field_summary)) {
            $summaries[$field_name] = $field_summary;
          }
        }
      }
    }

    return $summaries;
  }

  // ===========================================================================
  // Protected API
  // ===========================================================================

  /**
   * Gets the entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   The entity display repository interface.
   */
  protected function getEntityDisplayRepository(): EntityDisplayRepositoryInterface {
    return $this->entityDisplayRepository;
  }

  /**
   * Gets a summarizer for a particular field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to summarize.
   *
   * @return \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerInterface
   *   The summarizer to use for the field.
   */
  protected function getSummarizerFor(FieldDefinitionInterface $field_definition) {
    $factory = $this->fieldSummarizerFactory;

    return $factory->getSummarizerFor($field_definition);
  }

}
