<?php

namespace Drupal\inline_media_form\EntitySummarizer;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for objects that can provide a summary of an entity object.
 */
interface EntitySummarizerInterface {

  /**
   * Determines whether this summarizer can summarize the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @return bool
   *   TRUE if this summarize can summarize the entity; or, FALSE, otherwise.
   */
  public static function canHandle(EntityInterface $entity): bool;

  /**
   * Gets a human-friendly summary of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to summarize.
   * @param string $form_mode
   *   The form mode to use to drive the summary.
   *
   * @return string
   *   The human-friendly summary.
   */
  public function summarize(EntityInterface $entity,
                            string $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): string;

  /**
   * Gets human-friendly summaries of all fields of an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to summarize.
   * @param string $form_mode
   *   The form mode to use to drive the summary.
   *
   * @return string[]
   *   The human-friendly summaries of every field, keyed by field machine name.
   */
  public function summarizeFields(EntityInterface $entity,
                                  string $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): array;

}
