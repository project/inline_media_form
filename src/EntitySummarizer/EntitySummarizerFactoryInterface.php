<?php

namespace Drupal\inline_media_form\EntitySummarizer;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for factories that build entity summarizers.
 */
interface EntitySummarizerFactoryInterface {

  /**
   * Gets a summarizer for a particular entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which a summarizer is needed.
   *
   * @return \Drupal\inline_media_form\EntitySummarizer\EntitySummarizerInterface
   *   The summarizer to user for the entity.
   */
  public function getSummarizerFor(EntityInterface $entity): EntitySummarizerInterface;

}
