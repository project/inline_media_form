<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Interface for factories that build entity summarizers.
 */
interface FieldSummarizerFactoryInterface {

  /**
   * Gets a summarizer for a particular field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to summarize.
   *
   * @return \Drupal\inline_media_form\FieldSummarizer\FieldSummarizerInterface
   *   The summarizer to use for the field.
   */
  public function getSummarizerFor(FieldDefinitionInterface $field_definition): FieldSummarizerInterface;

}
