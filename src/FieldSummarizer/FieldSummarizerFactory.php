<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Default factory for field summarizers.
 */
class FieldSummarizerFactory implements FieldSummarizerFactoryInterface {

  // ===========================================================================
  // Constants
  // ===========================================================================

  /**
   * The field summarizers to consult.
   *
   * @var string[]
   */
  private const IMPLEMENTATIONS = [
    TextFieldSummarizer::class,
    FileFieldSummarizer::class,
    EntityReferenceFieldSummarizer::class,
    LinkFieldSummarizer::class,
    BlockFieldSummarizer::class,
    NoOpFieldSummarizer::class,
  ];

  // ===========================================================================
  // Member Fields
  // ===========================================================================

  /**
   * Drupal's renderer interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The repository of entity display information.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The repository of entities.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  // ===========================================================================
  // Constructor
  // ===========================================================================

  /**
   * Constructor for FieldSummarizerFactory.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Drupal's renderer interface.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The repository of entity display information.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The repository of entities in Drupal.
   */
  public function __construct(RendererInterface $renderer,
                              EntityDisplayRepositoryInterface $entity_display_repository,
                              EntityRepositoryInterface $entity_repository) {
    $this->renderer                = $renderer;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityRepository        = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummarizerFor(FieldDefinitionInterface $field_definition): FieldSummarizerInterface {
    foreach (static::IMPLEMENTATIONS as $class_name) {
      // @codingStandardsIgnoreLine
      /** @noinspection PhpUndefinedMethodInspection */
      if ($class_name::canHandle($field_definition)) {
        return new $class_name(
          $this->renderer,
          $this->entityDisplayRepository,
          $this->entityRepository
        );
      }
    }

    throw new \InvalidArgumentException(
      'Unsupported field type: ' . $field_definition->getType()
    );
  }

}
