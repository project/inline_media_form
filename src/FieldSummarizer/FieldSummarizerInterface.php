<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Interface for objects that can provide a summary of the contents of a field.
 */
interface FieldSummarizerInterface {

  /**
   * Determines whether this summarizer can summarize the given field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to summarize.
   *
   * @return bool
   *   TRUE if this summarize can summarize the field; or, FALSE, otherwise.
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool;

  /**
   * Gets a human-friendly summary of the contents of a field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_values
   *   The values of the field to summarize.
   * @param string $langcode
   *   The language code for which the summary is desired.
   *
   * @return string
   *   The human-friendly summary.
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string;

}
