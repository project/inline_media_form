<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Abstract base class for field summarizers.
 */
abstract class FieldSummarizerBase implements FieldSummarizerInterface {

  // ===========================================================================
  // Constants
  // ===========================================================================

  /**
   * The maximum length of a file field summary.
   */
  const MAX_SUMMARY_LENGTH = 150;

  // ===========================================================================
  // Member Fields
  // ===========================================================================

  /**
   * Drupal's renderer interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The repository of entity display information.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The repository of entities.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  // ===========================================================================
  // Constructor
  // ===========================================================================

  /**
   * Constructor for FieldSummarizerBase.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Drupal's renderer interface.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The repository of entity display information.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The repository of entities in Drupal.
   */
  public function __construct(RendererInterface $renderer,
                              EntityDisplayRepositoryInterface $entity_display_repository,
                              EntityRepositoryInterface $entity_repository) {
    $this->renderer                = $renderer;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityRepository        = $entity_repository;
  }

  // ===========================================================================
  // Protected Accessors
  // ===========================================================================

  /**
   * Gets the renderer interface.
   *
   * @return \Drupal\Core\Render\RendererInterface
   *   The text rendering interface.
   */
  protected function getRenderer(): RendererInterface {
    return $this->renderer;
  }

  /**
   * Gets the entity display repository.
   *
   * @return \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   *   The entity display repository interface.
   */
  protected function getEntityDisplayRepository(): EntityDisplayRepositoryInterface {
    return $this->entityDisplayRepository;
  }

  /**
   * Gets the entity repository.
   *
   * @return \Drupal\Core\Entity\EntityRepositoryInterface
   *   The entity repository.
   */
  protected function getEntityRepository(): EntityRepositoryInterface {
    return $this->entityRepository;
  }

  // ===========================================================================
  // Protected API
  // ===========================================================================

  /**
   * Shortens summary text to a maximum length.
   *
   * @param string $summary
   *   The summary to shorten.
   *
   * @return string
   *   The shortened summary.
   */
  protected function shortenSummary(string $summary): string {
    if (strlen($summary) > self::MAX_SUMMARY_LENGTH) {
      $short_summary = Unicode::truncate($summary, self::MAX_SUMMARY_LENGTH);
    }
    else {
      $short_summary = $summary;
    }

    return $short_summary;
  }

}
