<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Markup;

/**
 * Summarizer for text fields.
 */
class TextFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();
    $field_name = $field_definition->getName();

    $text_types = [
      'text_with_summary',
      'text',
      'text_long',
      'list_string',
      'string',
    ];

    $excluded_text_types = [
      'parent_id',
      'parent_type',
      'parent_field_name',
    ];

    $is_text     = in_array($field_type, $text_types);
    $is_excluded = in_array($field_name, $excluded_text_types);

    return ($is_text && !$is_excluded);
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    $summary_items = [];

    foreach ($field_values as $field_value) {
      if (isset($field_value)) {
        $text = $field_value->value;

        if (!empty($text)) {
          $summary_items[] = $text;
        }
      }
    }

    $summary = implode(', ', $summary_items);

    return Markup::create($this->shortenSummary(trim($summary)));
  }

}
