<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\block_field\BlockFieldItemInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Summarizer for block reference fields.
 */
class BlockFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();

    return ($field_type == 'block_field');
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    assert($field_values instanceof BlockFieldItemInterface);

    $summary_items = [];

    foreach ($field_values as $field_value) {
      $block = $field_value->getBlock();

      if (!empty($block)) {
        $summary_items[] = $block->getPluginDefinition()['admin_label'];
        break;
      }
    }

    $summary = implode(', ', $summary_items);

    return $this->shortenSummary($summary);
  }

}
