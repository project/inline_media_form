<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Summarizer for entity reference fields.
 */
class EntityReferenceFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();

    return ($field_type == 'entity_reference');
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    assert($field_values instanceof EntityReferenceFieldItemListInterface);

    $summary_items       = [];
    $referenced_entities = $field_values->referencedEntities();

    foreach ($referenced_entities as $referenced_entity) {
      if (!$referenced_entity->access('view label')) {
        continue;
      }

      $entity_repository = $this->getEntityRepository();

      // Switch to the entity translation in the current context.
      $entity =
        $entity_repository->getTranslationFromContext(
          $referenced_entity,
          $langcode
        );

      $summary_items[] = $entity->label();
    }

    $summary = implode(', ', $summary_items);

    return $this->shortenSummary(trim($summary));
  }

}
