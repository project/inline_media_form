<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Fallback, no-op field summarizer that always returns an empty string.
 */
class NoOpFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    // Accept all fields.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    return '';
  }

}
