<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;

/**
 * Summarizer for file and image fields.
 */
class FileFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();

    return in_array($field_type, ['image', 'file']);
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    assert($field_values instanceof FileFieldItemList);

    $summary_items = [];

    foreach ($field_values as $file_value) {
      $text = '';

      $candidate_values = [
        $file_value->description ?? NULL,
        $file_value->title ?? NULL,
        $file_value->alt ?? NULL,
      ];

      if (isset($file_value->entity)) {
        $candidate_values[] = $file_value->entity->getFileName();
      }

      foreach ($candidate_values as $value) {
        if (!empty($value)) {
          $text = $value;
          break;
        }
      }

      if (!empty($text)) {
        $summary_items[] = $text;
      }
    }

    $summary = implode(', ', $summary_items);

    return $this->shortenSummary(trim($summary));
  }

}
