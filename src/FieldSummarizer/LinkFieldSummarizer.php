<?php

namespace Drupal\inline_media_form\FieldSummarizer;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Summarizer for link fields.
 */
class LinkFieldSummarizer extends FieldSummarizerBase {

  /**
   * {@inheritdoc}
   */
  public static function canHandle(FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();

    return ($field_type == 'link');
  }

  /**
   * {@inheritdoc}
   */
  public function summarize(FieldItemListInterface $field_values,
                            string $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED): string {
    $summary_items = [];

    foreach ($field_values as $link_value) {
      $text = '';

      $candidate_values = [
        $link_value->title ?? NULL,
        $link_value->uri ?? NULL,
      ];

      foreach ($candidate_values as $value) {
        if (!empty($value)) {
          $text = $value;
          break;
        }
      }

      if (!empty($text)) {
        $summary_items[] = $text;
      }
    }

    $summary = implode(', ', $summary_items);

    return $this->shortenSummary(trim($summary));
  }

}
