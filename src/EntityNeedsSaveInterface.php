<?php

namespace Drupal\inline_media_form;

/**
 * Interface for entities that can be marked dirty (i.e. have unsaved changes).
 *
 * Adapted from \Drupal\entity_reference_revisions\EntityNeedsSaveInterface.
 */
interface EntityNeedsSaveInterface {

  /**
   * Gets whether this entity needs to be saved.
   *
   * @return bool
   *   TRUE if this entity needs to be saved; or, FALSE, otherwise.
   */
  public function isSaveNeeded();

  /**
   * Marks this entity dirty/in need of a save.
   */
  public function markSaveNeeded();

  /**
   * Marks this entity dirty/in need of a save.
   */
  public function markSaved();

}
