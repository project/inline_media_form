<?php

namespace Drupal\inline_media_form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\media\MediaInterface;

/**
 * An interface for classes that adapt media entities to work with IMF widgets.
 *
 * This is needed to overcome some important differences between Media entities
 * and Paragraph entities, since the code for IMF widgets is based on approaches
 * borrowed from the Paragraphs module.
 */
interface MediaItemAdapterInterface extends EntityNeedsSaveInterface {

  /**
   * Gets the type of media entity (the bundle).
   *
   * @return string
   *   The bundle of the entity this adapter wraps.
   */
  public function getMediaType(): string;

  /**
   * Returns whether this entity was modified since its last saved revision.
   *
   * @return bool
   *   TRUE if this entity has unsaved changes; or, FALSE, otherwise.
   *
   * @see \Drupal\Core\Entity\ContentEntityBase::hasTranslationChanges()
   */
  public function isChanged(): bool;

  /**
   * Returns whether this entity has been published for non-admins to see.
   *
   * @return bool
   *   TRUE if this entity has been published; or, FALSE, if it is only visible
   *   to those with permission to see unpublished media files.
   *
   * @see \Drupal\Core\Entity\EntityPublishedInterface::isPublished()
   */
  public function isPublished(): bool;

  /**
   * Determines whether the logged-in user can view the media entity.
   *
   * @return bool
   *   TRUE if the logged-in user can view the entity this adapter wraps.
   */
  public function canView(): bool;

  /**
   * Determines whether the logged-in user can modify the media entity.
   *
   * @return bool
   *   TRUE if the logged-in user can modify the entity this adapter wraps.
   */
  public function canModify(): bool;

  /**
   * Determines whether the logged-in user can delete the media entity.
   *
   * @return bool
   *   TRUE if the logged-in user can delete the entity this adapter wraps.
   */
  public function canDelete(): bool;

  /**
   * Saves this media entity, if it has been marked dirty/in need of a save.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the entity fails to save successfully.
   */
  public function saveIfNecessary(): void;

  /**
   * Returns a short, human-friendly summary of the media entity.
   *
   * @param string $form_mode
   *   The form mode to use to drive the summary.
   *
   * @return string
   *   A human-friendly summary of the entity this adapter wraps.
   */
  public function toSummary(string $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): string;

  /**
   * Returns human-friendly summaries of the fields of the media entity.
   *
   * Each item summarizes the value of a single field of the entity.
   *
   * @param string $form_mode
   *   The form mode to use to drive the summary.
   *
   * @return string[]
   *   A list of field summary items.
   */
  public function toFieldSummaries(string $form_mode = EntityDisplayRepositoryInterface::DEFAULT_DISPLAY_MODE): array;

  /**
   * Converts this adapter into the media entity it's wrapping.
   *
   * @return \Drupal\media\MediaInterface
   *   The media entity being wrapped by this adapter.
   */
  public function toMediaEntity(): MediaInterface;

}
